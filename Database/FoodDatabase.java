package Database;

import food.*;

import java.util.*;

public class FoodDatabase {
    private Map<Cuisine, List<FoodItem>> foodItemsByCuisine;
    private Map<Cuisine, List<FoodItem>> dessertItemsByCuisine;
    private List<FoodItem> drinks;

    public Map<Cuisine, List<FoodItem>> getFoodItemsByCuisine() {
        return foodItemsByCuisine;
    }

    public Map<Cuisine, List<FoodItem>> getDessertItemsByCuisine() {
        return dessertItemsByCuisine;
    }

    public List<FoodItem> getDrinks() {
        return drinks;
    }

    public FoodDatabase() {
        this.foodItemsByCuisine = new HashMap<>();
        this.dessertItemsByCuisine = new HashMap<>();
        this.drinks = new ArrayList<>();
        for (Cuisine cuisine : Cuisine.values()) {
            foodItemsByCuisine.put(cuisine, new ArrayList<>());
        }
        for (Cuisine cuisine : Cuisine.values()) {
            dessertItemsByCuisine.put(cuisine, new ArrayList<>());
        }
        populateDatabase();
    }

    private void populateDatabase() {

        // cuisine-specific menu initialization in two maps: for main meals, and for desserts
        foodItemsByCuisine.get(Cuisine.POLISH).addAll(getPolishItems());
        foodItemsByCuisine.get(Cuisine.MEXICAN).addAll(getMexicanItems());
        foodItemsByCuisine.get(Cuisine.ITALIAN).addAll(getItalianItems());

        dessertItemsByCuisine.get(Cuisine.POLISH).addAll(getPolishDessertItems());
        dessertItemsByCuisine.get(Cuisine.MEXICAN).addAll(getMexicanDessertItems());
        dessertItemsByCuisine.get(Cuisine.ITALIAN).addAll(getItalianDessertItems());

        drinks.addAll(getDrinkItems());
    }

    private Collection<? extends FoodItem> getDrinkItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Drink("Vodka", 6.66));
        items.add(new Drink("Water", 0.0));
        items.add(new Drink("Sprite", 1.26));
        items.add(new Drink("Cola", 1.26));
        items.add(new Drink("Apple Juice", 4.1));
        items.add(new Drink("Tomato Juice", 3.8));
        items.add(new Drink("Tekila", 10.13));
        items.add(new Drink("Biherovka", 12.5));
        items.add(new Drink("Bud light beer", 5.2));
        items.add(new Drink("Budwaiser beer", 5.2));
        items.add(new Drink("Tea", 2.4));
        items.add(new Drink("Coffe espresso", 5.4));
        items.add(new Drink("Coffe latte ", 6.2));
        return items;
    }

    private List<FoodItem> getPolishItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Meal("Pierogi", 10.99, Cuisine.POLISH));
        items.add(new Meal("Bigos", 12.49, Cuisine.POLISH));
        items.add(new Meal("Kielbasa", 8.99, Cuisine.POLISH));
        return items;
    }

    private List<FoodItem> getMexicanItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Meal("Tacos", 7.49, Cuisine.MEXICAN));
        items.add(new Meal("Burrito", 9.99, Cuisine.MEXICAN));
        items.add(new Meal("Quesadilla", 6.99, Cuisine.MEXICAN));
        return items;
    }

    private List<FoodItem> getItalianItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Meal("Pizza", 11.99, Cuisine.ITALIAN));
        items.add(new Meal("Pasta Carbonara", 13.99, Cuisine.ITALIAN));
        items.add(new Meal("Caesar salad", 5.99, Cuisine.ITALIAN));
        return items;
    }

    private List<FoodItem> getPolishDessertItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Dessert("Paczki", 4.99, Cuisine.POLISH));
        items.add(new Dessert("Sernik", 6.49, Cuisine.POLISH));
        return items;
    }

    private List<FoodItem> getMexicanDessertItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Dessert("Churros", 5.99, Cuisine.MEXICAN));
        items.add(new Dessert("Flan", 4.99, Cuisine.MEXICAN));
        return items;
    }

    private List<FoodItem> getItalianDessertItems() {
        List<FoodItem> items = new ArrayList<>();

        // add more items to a menu
        items.add(new Dessert("Tiramisu", 5.99, Cuisine.ITALIAN));
        items.add(new Dessert("Cannoli", 4.49, Cuisine.ITALIAN));
        return items;
    }

}
