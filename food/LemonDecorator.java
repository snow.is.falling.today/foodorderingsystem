package food;

public class LemonDecorator extends FoodItem{
    private FoodItem drink;

    public LemonDecorator(FoodItem drink) {
        this.drink = drink;
        this.name = drink.getName() + " with Lemon";
        price = drink.getPrice() + 0.3;
    }
}
