package food;


public class Meal extends FoodItem {

    public Meal(String name, double price, Cuisine cuisine) {
        this.name = name;
        this.price = price;
        this.cuisine = cuisine;
    }
}
