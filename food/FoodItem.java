package food;

public abstract class FoodItem {
    protected String name;
    protected double price;
    protected Cuisine cuisine;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }
}
