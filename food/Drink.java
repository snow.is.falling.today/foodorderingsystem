package food;

public class Drink extends FoodItem{

    public Drink(String name, double price) {
        this.name = name;
        this.price = price;
    }
}
