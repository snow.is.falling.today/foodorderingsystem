package food;

public class IceCubesDecorator extends FoodItem{
    private FoodItem drink;

    public IceCubesDecorator(FoodItem drink) {
        this.drink = drink;
        this.name = drink.getName() + " with Ice Cubes";
        price = drink.getPrice() + 0.5;
    }
}
