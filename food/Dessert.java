package food;

public class Dessert extends FoodItem{
    public Dessert(String name, double price, Cuisine cuisine) {
        this.name = name;
        this.price = price;
        this.cuisine = cuisine;
    }
}
