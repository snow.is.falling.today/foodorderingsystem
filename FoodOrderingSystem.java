import DAO.FoodItemDAO;
import food.Cuisine;
import food.FoodItem;
import java.util.List;

// Created by conditions(1) from https://www.x-formation.com/wp-content/uploads/2021/04/RECRUITMENT_TEST_JAVA.pdf

public class FoodOrderingSystem {
    private FoodItemDAO foodItemDAO;

    public FoodOrderingSystem() {
        foodItemDAO = new FoodItemDAO();
    }

    public List<FoodItem> getFoodItemsByCuisine(Cuisine cuisine) {
        return foodItemDAO.getAllFoodItemsByCuisine(cuisine);
    }

    public List<FoodItem> getDessertsByCuisine(Cuisine cuisine) {
        return foodItemDAO.getAllDessertItemsByCuisine(cuisine);
    }

    public List<FoodItem> getDrinks() {
        return foodItemDAO.getDrinks();
    }

    public static void main(String[] args) {
        FoodOrderingSystem orderingSystem = new FoodOrderingSystem();
        UserInteraction userInteraction = new UserInteraction(orderingSystem);
        userInteraction.start();
    }
}
