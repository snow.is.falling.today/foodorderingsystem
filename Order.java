import food.FoodItem;

import java.util.ArrayList;
import java.util.List;

public class Order {

    // to save an order status for customers
    private List<FoodItem> items = new ArrayList<>();

    public void addItem(FoodItem item) {
        items.add(item);
    }

    public List<FoodItem> getItems() {
        return items;
    }

    public double getTotalPrice() {
        double totalPrice = 0.0;
        for (FoodItem item : items) {
            totalPrice += item.getPrice();
        }
        return totalPrice;
    }
}
