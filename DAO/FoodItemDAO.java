package DAO;

import Database.FoodDatabase;
import food.Cuisine;
import food.FoodItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FoodItemDAO {
    FoodDatabase foodDatabase;
    private Map<Cuisine, List<FoodItem>> foodItemsByCuisine;
    private Map<Cuisine, List<FoodItem>> dessertItemsByCuisine;
    private List<FoodItem> drinks;


    public FoodItemDAO() {
        this.foodDatabase = new FoodDatabase();
        this.foodItemsByCuisine = foodDatabase.getFoodItemsByCuisine();
        this.dessertItemsByCuisine = foodDatabase.getDessertItemsByCuisine();
        this.drinks = foodDatabase.getDrinks();
    }

    public void addFoodItem(FoodItem foodItem) {
        foodItemsByCuisine.get(foodItem.getCuisine()).add(foodItem);
    }

    public void addDessertItem(FoodItem foodItem) {
        dessertItemsByCuisine.get(foodItem.getCuisine()).add(foodItem);
    }

    public List<FoodItem> getAllFoodItemsByCuisine(Cuisine cuisine) {
        return new ArrayList<>(foodItemsByCuisine.get(cuisine));
    }

    public List<FoodItem> getAllDessertItemsByCuisine(Cuisine cuisine) {
        return new ArrayList<>(dessertItemsByCuisine.get(cuisine));
    }

    public List<FoodItem> getDrinks() {
        return drinks;
    }
}
