import food.Cuisine;
import food.FoodItem;
import food.IceCubesDecorator;
import food.LemonDecorator;

import java.util.List;
import java.util.Scanner;

public class UserInteraction {
    private FoodOrderingSystem orderingSystem;
    private Scanner scanner;
    private Order order;

    public UserInteraction(FoodOrderingSystem orderingSystem) {
        this.orderingSystem = orderingSystem;
        scanner = new Scanner(System.in);
        order = new Order();
    }

    public void start() {
        System.out.println("Welcome to the Food Ordering System!");
        while (true) {
            displayMainMenu();
            int choice = getUserChoice(1, 6);
            switch (choice) {
                case 1 -> orderMeal();
                case 2 -> orderDessert();
                case 3 -> orderDrink();
                case 4 -> displayAvailableItems();
                case 5 -> showOrderDetails();
                case 6 -> {
                    System.out.println("Thank you for using the Food Ordering System!");
                    scanner.close();
                    return;
                }
            }
        }
    }
    private void displayMainMenu() {
        System.out.println("Main Menu:");
        System.out.println("1. Order a Meal");
        System.out.println("2. Order a Dessert");
        System.out.println("3. Order a Drink");
        System.out.println("4. Display Available Items");
        System.out.println("5. Show order details");
        System.out.println("6. Exit");
    }

    private int getUserChoice(int min, int max) {
        int choice;
        while (true) {
            System.out.print("Enter your choice: ");
            try {
                choice = Integer.parseInt(scanner.nextLine());
                if (choice >= min && choice <= max) {
                    return choice;
                } else {
                    System.out.println("Invalid choice. Please enter a valid option.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number.");
            }
        }
    }

    private Cuisine getCuisineChoice() {
        for (Cuisine cuisine : Cuisine.values()) {
            System.out.println(cuisine.ordinal() + 1 + ". " + cuisine);
        }
        int choice = getUserChoice(1, Cuisine.values().length);
        return Cuisine.values()[choice - 1];
    }

    private FoodItem getFoodItemChoice(Cuisine cuisine) {
        List<FoodItem> items = orderingSystem.getFoodItemsByCuisine(cuisine);
        int choice = getUserChoice(1, items.size());
        return items.get(choice - 1);
    }

    private FoodItem getDessertChoice(Cuisine cuisine) {
        List<FoodItem> items = orderingSystem.getDessertsByCuisine(cuisine);
        int choice = getUserChoice(1, items.size());
        return items.get(choice - 1);
    }

    private FoodItem getDrinksChoice() {
        List<FoodItem> items = orderingSystem.getDrinks();
        int choice = getUserChoice(1, items.size());
        return items.get(choice - 1);
    }

    private void orderMeal() {
        System.out.println("Choose a cuisine:");
        Cuisine cuisine = getCuisineChoice();
        displayFoodItemsByCuisine(cuisine);
        order.addItem(getFoodItemChoice(cuisine));
    }

    private void orderDessert() {
        System.out.println("Choose a cuisine:");
        Cuisine cuisine = getCuisineChoice();
        displayDessertByCuisine(cuisine);
        order.addItem(getDessertChoice(cuisine));
    }

    private void orderDrink() {
        displayDrinks();
        FoodItem drinkChoice = getDrinksChoice();
        // decorate with ice cubes
        System.out.println("Would you like to add ice cubes to your drink?");
        System.out.println("1. Yes");
        System.out.println("2. No");
        int choiceIceCube = getUserChoice(1, 2);

        // decorate with a lemon
        System.out.println("Would you like to add a lemon to your drink?");
        System.out.println("1. Yes");
        System.out.println("2. No");
        int choiceLemon = getUserChoice(1, 2);

        if (choiceIceCube == 1 && choiceLemon == 1) {
            order.addItem(new IceCubesDecorator(new LemonDecorator(drinkChoice)));
        } else if (choiceIceCube == 1) {
            order.addItem(new IceCubesDecorator(drinkChoice));
        } else if (choiceLemon == 1) {
            order.addItem(new LemonDecorator(drinkChoice));
        } else {
            order.addItem(drinkChoice);
        }
    }

    private void displayAvailableItems() {
        System.out.println("Choose a cuisine");
        Cuisine cuisine = getCuisineChoice();
        displayFoodItemsByCuisine(cuisine);
        displayDessertByCuisine(cuisine);
        displayDrinks();
    }

    private void displayFoodItemsByCuisine(Cuisine cuisine) {
        System.out.println("Available " + cuisine + " items:");
        List<FoodItem> items = orderingSystem.getFoodItemsByCuisine(cuisine);
        int counter = 1;
        for (FoodItem item : items) {
            System.out.println(counter++ + ". " + item.getName() + " - $" + item.getPrice());
        }
        System.out.println();
    }

    private void displayDessertByCuisine(Cuisine cuisine) {
        System.out.println("Available " + cuisine + " items:");
        List<FoodItem> items = orderingSystem.getDessertsByCuisine(cuisine);
        int counter = 1;
        for (FoodItem item : items) {
            System.out.println(counter++ + ". " + item.getName() + " - $" + item.getPrice());
        }
        System.out.println();
    }

    private void displayDrinks() {
        System.out.println("Available drinks:");
        List<FoodItem> items = orderingSystem.getDrinks();
        int counter = 1;
        for (FoodItem item : items) {
            System.out.println(counter++ + ". " + item.getName() + " - $" + item.getPrice());
        }
        System.out.println();
    }

    public void showOrderDetails() {
        System.out.println("==============");
        System.out.println("Order details:");
        for (FoodItem item : order.getItems()) {
            System.out.println(item.getName() + " - $" + item.getPrice());
        }
        System.out.println("Total Price: $" + order.getTotalPrice());
        System.out.println("==============");
    }
}
